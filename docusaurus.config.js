module.exports = {
  title: 'NanoKarrin x Pyrkon 2020',
  tagline: 'Działalność NanoKarrin na Pyrkonie 2020 ',
  url: 'https://pyrkon-doc.now.sh/',
  baseUrl: '/',
  favicon: 'img/favicon.ico',
  organizationName: 'NanoKarrin', // Usually your GitHub org/user name.
  projectName: 'NanoKarrin x Pyrkon 2020', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'NK x Pyrkon',
      logo: {
        alt: 'NanoKarrin',
        src: 'img/logo.png',
      },
      links: [
        {to: 'docs/ogolne', label: 'Dokumentacja', position: 'left'},
        {
          href: 'https://nanokarrin.pl/',
          label: 'Strona NanoKarrin',
          position: 'right',
        },
        {
          href: 'https://pyrkon.pl/',
          label: 'Strona Pyrkonu',
          position: 'right',
        },
      ],
    }
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://gitlab.com/nanokarrin/konwenty/pyrkon-doc/-/edit/master/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
