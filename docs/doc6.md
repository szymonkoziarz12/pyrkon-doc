---
id: warsztaty
title: Warsztaty
sidebar_label: Warsztaty
---

### Warsztaty wokalne
W stylu śpiewań a cappella, które robiliśmy na poprzednich konwentach.  
Warsztaty poprowadzi Wengiel, natomiast potrzebne będą osoby, które chciałyby zostać liderami grup wokalnych/grupy perkusyjnej.  
Zgłoszenia przez formularz.


### Warsztaty tekściarskie / Piosenka Pyrkonowa
Warsztaty prowadzi Serek. Po części teoretycznej następuje wspólne tworzenie Piosenki Pyrkonowej 2020.   
Czy jest reżyser, który chciałby po konwencie przygotować "studyjną" wersję tej piosenki, żeby potem wrzucić ją na YT i Spotify?