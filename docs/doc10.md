---
id: poprzedni
title: Wasze wnioski 2019
sidebar_label: Wasze wnioski 2019
---

### Wybrane cytaty z anonimowej ankiety
:::important
Wklejam je, żebyście wiedzieli, natomiast ja nie za bardzo mam czas przyłożyć rękę do wszystkiego, więc jeśli kogoś poruszy któraś sprawa / któryś problem i będzie chciał zająć się rozwiązniem, niech się do mnie zgłosi.
:::important

> "dajcie marker od razu przypisany do mapy"

> "długopisów do list czasem brakowało"

> "Co myślicie o zrobieniu tabliczki na patyku do wołania nowej osoby w kolejce? Albo może dzwonek?"

> "Zróbcie widoczne ogłoszenie na stoisku rysowniczym, żeby skończone obrazki przyczepiać do ścianki"

> "więcej osób do nagabywania jest potrzebne albo jedna osoba do wychodzenia do ludzi, a jedna do pilnowania listy"

> "Może mapka w bardziej widocznym miejscu? Żeby ludzie nie zapominali!"

> "przydałaby się instrukcja jak włączyć sprzęt - kopiowanie ekranów etc., najlepiej przed Pyrkonem"

> "zrobicie techniczną listę otwierania stoiska i zdawania stoiska?"

> "Problemem, moim zdaniem, było zgromadzenie nadmiernej ilości osób z grupy wokół stoiska w sposób uniemożliwiający ruch... lub po prostu zawstydzający osoby spoza grupy do podejścia”


>"Brakowało statywu do mikrofonu dla osoby obsługującej stanowisko przez co musiała go trzymać sama, co przy dużej ilości klikania marnowało dużo czasu”


>"Można stworzyc księgę nk do ktorej mogłaby sie wpisac każda osoba z nk będąca na danym konwencie”


>"Kupcie stojaczek na wizytówki!”

