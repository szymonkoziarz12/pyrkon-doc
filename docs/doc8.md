---
id: media
title: Media
sidebar_label: Media
---

W zeszłym roku mieliśmy bardzo mało fotek stoiska i procesu nagrywania. W tym roku trochę to uporządkujemy, dlatego wyznaczyłam Orzecha na koordynatora mediów. To na jego barkach spoczywać będzie odpowiedzialność medialna.  
Jednakże sam nie da rady i na pewno będzie potrzebować waszej pomocy. W końcu jeśli każdy członek NK zrobi jedno zdjęcie, to już będziemy mieć wspaniałą galerię.  
Dokładniejsza rozpiska potencjalnych działań medialnych znajduje się w formularzu zgłoszeniowym. Są to m.in. zdjęcia docelowe na fb, na insta, pilnowanie instaramki czy przygotowanie zajawki Pyrkonowej na YT.  
