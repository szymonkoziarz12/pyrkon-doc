---
id: art
title: Kącik rysowniczy
sidebar_label: Wystrój i kącik rysowniczy
---

### Wystrój
Za tę działkę odpowiada Kyofu. Tutaj wpada przygotowanie grafik na konwent, roll upu, plakatów, wizytowek, kołoinformacji... Do tego dochodzi wystrój stoiska, czyli zorganizowanie materiałów na ścianki, obrusów i rozłożenie wszystkiego w czwartek / piątek.  
Roboty od groma, sama tego nie zrobi. Jeśli są osoby, które lubią zajmować się takimi sprawami, proszę, napiszcie do Kyofu i spytajcie, jak możecie pomóc. Jeśli ktoś nie ma zmysłu artystycznego, ale jest wysoki, może pomóc i np. umocować materiał na ściankach.

### Kącik rysowniczy


Jak w poprzednich latach, jedną ławkę przeznaczymy na kącik rysowniczy. Będą tam blok rysunkowy, taśma klejąca (przezroczysta) oraz przybory do rysowania (kredki, flamastry).  
Zadanie polega na zorganizowaniu powyższych przedmiotów i dostarczeniu je na stoisko w piątek do południa. Jeśli możecie coś przynieść, wpiszcie to w formularz.

### Galeria
Na ściankach naszego stoiska będziemy, jak rok temu, przyklejać plakaty-wydruki rysunków członków NK oraz rysunki wszystkich, którzy w ciągu Pyrkonu stworzą coś na stoisku NK.  
Zadanie polega na przyniesieniu na Pyrkon swoich rysunków. Zostaną one przymocowane do ścianek (najpewniej) szpilkami i będą do odebrania po konwencie (jeśli ktoś by chciał zwrot). 

:::important
@Ilustrator NK @Animator NK  
Z tego, co wiem, obrazy/animacje do projektów NK tworzycie w wysokich rozdzielczościach. Przemyślcie czy nie chcecie wydrukować na Pyrkon kolażu narysowanych do projektów postaci  lub screenów 1-2 kadrów z animacji. Śmiało możecie na taki wydruk dać też swój nick (używany w NK) i jakiś namiar na siebie, żeby się poreklamować.
:::important