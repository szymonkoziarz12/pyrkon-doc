---
id: wd
title: Wolne Dubbingowanie
sidebar_label: Wolne Dubbingowanie
---

### Nagrywamy

Tutaj workflow dzieli się na trzy części: punkt info / rozmowy; reżyserię; nagrywanie.

##### Punkt info

Jedna osoba powinna zawsze siedzieć lub stać za ławką w punkcie info. Druga z gadających może się kręcić wśród tłumu. Sami decydujecie, jak się dzielicie, ale nie może być tak, że za ławką nikt nie siedzi.  
Nagabywacze subtelnie wyłapują w tłumie tych, którzy wyglądają na zaciekawionych. Tłumaczą, co to za stoisko, co robimy, jakie są możliwości na konwencie.  
Osoby siedzące pełnią ważną funkcję zawiadywania chętnymi do nagrań. W 2020 r. będzie jedno stanowisko, scenkowo-piosenkowe, a więc i jedna lista. To te osoby patrzą, czy nick jest wpisany wyraźnie (by dało się go rozczytać i wywołać). To one przekazują kolejnego konwentowicza z listy reżyserowi. To one WYKREŚLAJĄ z listy konwentowicza, który siada przed mikrofonem.  
Zasady kolejkowe są proste: 
* jedna osoba może nagrać się pod jedną postać w scence
* lub może nagrać jedną piosenkę (przy czym można mieć max. 2-3 podejścia do piosenki, bo to mocno blokuje stanowisko)

Gdy reżyser skończy pracę z konwentowiczem, daje znak punktowi info (najlepiej, by podszedł, bo hałas). Tam dyżurujący członek NK wyczytuje ludzi po kolei z listy i przekazuje kolejną osobę reżyserowi.  
Jeśli ktoś się wpisze na listę i ucieknie ze stoiska, mówiąc, że wróci za jakiś czas, to **nie skreślamy go z listy**, a po powrocie jest przyjmowany w pierwszej kolejności (nie został wykreślony z listy, więc jest wyczytywany jako pierwszy każdorazowo).  

NIE prosimy o nr telefonów i NIE wysyłamy SMS-ów z wiadomością “jesteś drugi w kolejce”. Ten system sprawdzał się na małych konwentach, gdzie ludzie powiadomieni pojawiali się w minutę w naszej salce. Na Pyrkonie nie przychodzili nawet 15 minut, a nie ma co dla nich blokować stanowiska. Muszą czekać, trudno. Dodatkowo jeśli jakimś cudem obiecacie komuś SMS-a lub telefon, a skończy się wasz dyżur, to **zostajecie z tym obowiązkiem**, nie przechodzi on na kolejnych dyżurujących - a zakładam, że wolelibyście po prostu sobie pójść  ;).  

Jeśli kolejki nie ma i dyżurujący z punktu info się upewni (czyli spyta się na głos np. “Czy ktoś czeka na nagrania?”, bo czasami ludzie czają się w okolicy i czekają na to pytanie), to konwentowicz może nagrać kolejną rzecz (znowu na zasadzie 2-3 prób, po których następuje sprawdzenie okolicy).  
Porady i informacje, jak rozmawiać z konwentowiczami i na co zwracać uwagę, możecie uzyskać od Serka. 

##### Reżyser
To osoba, która przejmuje konwentowicza z punktu info i sprawia, że konwentowicz jest na każdym kroku zaopiekowany. Reżyser przedstawia się i przedstawia technika (np. "Cześć, jestem Pchełka. Będę ci pomagać w nagraniach. Tutaj jest Orzecho, wesprze nas technicznie"). Reżyser siedzi przy konwentowiczu i doradza mu, co poprawić w kolejnym podejściu, żeby było lepiej, ale także **kontroluje czas**. Czasami konwentowicze utykają w jakimś miejscu, a czas leci. Reżyser wtedy określa, które z dotychczasowych podejść było najlepsze i proponuje przejście do kolejnej kwestii. Nowicjusze lepiej reagują, gdy ktoś im coś podpowie lub nawet stwierdzi "okej, dalej". Po nagraniu reżyser odpowiada na krótkie pytania konwentowicza, a jeśli są dłuższe - odsyła do punktu info z hasłem "Serek ci wszystko chętnie opowie", żeby nie blokować kolejki.

##### Technik
Nagrywający siedzą przy komputerze i ogarniają proces nagrywania. W tym roku jest tylko jedno stanowisko, więc trzeba dbać o to, by piosenki i scenki się nie mieszały. To technicy dbają o poprawne podpisywanie ścieżek.  
W piątek o 10.00 powinien być już rozstawiony cały sprzęt do nagrywania. Na komputerze stoiskowym zainstalowany zostanie Reaper, w którym będziecie zapisywać nagrania. Ogólne zasady się nie zmieniają, więc rokrocznie odsyłam do dwóch nagrań, jedno jest o scenkach, a drugie o piosenkach:
* [Scenki](https://drive.google.com/open?id=1pjM5CcX0ubaxKDB6NLQZr2aFxKB7HR8K)
* [Piosenki](https://drive.google.com/open?id=1TVCze9pnETW4N_5KldsL_xF8hf8GYZDa)

Pamiętajcie, że mamy teraz inne projekty i coś czuję, że nieco inaczej rozwiążemy sprawę piosenek, ale to zrobię konfę pod koniec kwietnia i wszystko wyjaśnię, stay tuned.
  Nie będziemy sprzętu montować tak, by puszczać nagrania na głośnikach - za dużo przełączania, a i tak nie słychać, bo głośno w hali. Nagraną scenkę/piosenkę prezentujemy nagrywającemu na słuchawkach i tyle.
* Piosenki wysyłamy jako surowe, nieobrobione nagrania samego głosu, w folderze zbiorczym na Facebooku; dołączymy podkłady, gdyby ktoś chciał to sobie zmiksować
* Scenki wrzucamy na YT jak w poprzednich latach; nie wysyłamy pojedynczych nagrań prywatnie

Pracujecie z konwentowiczami jeden na jeden, więc jeśli jakaś grupka chce nagrać, to każdy musi osobno, niech nie drą się wspólnie do jednego mikrofonu, bo potem burdel i to się nadaje tylko do kosza.  
Każde nagranie opisujecie PRZED nagraniem (bo po nagraniu ludzie uciekają, serio). Ścieżkę opiszcie nickiem, najlepiej wyjątkowym (kto by chciał 20 ścieżek “Ania”?), upewnijcie się, że został poprawnie zapisany, niech konwentowicz wam zerknie przez ramię.

### Co będzie dostępne podczas Wolnego Dubbingowania?

##### Dwie scenki
Jedna z Hanasaku Iroha - Home Sweet Home, druga z Uniwersytetu Potwornego. Koniecznie obejrzyjcie obie, żebyście wiedzieli, co polecać ludziom. Kierujcie ludźmi tak, by mieć nagrania na każdą rolę. Reżyserzy, pytajcie technika, czy zostały jeszcze jakieś nieobsadzone role!  
Plan jest taki, by po zakończeniu konwentu wydać wideo, w którym zamieścimy te scenki z głosami nagranymi podczas konwentu. Przykład:
<iframe width="560" height="315" src="https://www.youtube.com/embed/AKOW5k8vM70" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  
Dlatego też, jeśli ktoś zgadza się, by zapisać jego nagrania na komputerze NK, zgadza się na publikację swojego głosu - to ważne, by ludzie mieli tego świadomość. Tutaj musicie też im zaznaczyć, że chociaż sprzęt jest dobry, to dźwięki tła robią swoje.  
Jedna osoba może nagrać się do tylu postaci, do ilu chce, ale po każdej postaci musi stanąć na nowo w kolejce!  
Scenki będą dostępne z poziomu pulpitu, każda w swoim folderze razem ze scenariuszem. W tym samym folderze znajdziecie plik reaperowy, który będzie mieć już wrzuconą scenkę. Zanim zaczniecie nagrywać, zróbcie nową ścieżkę w grupie danej postaci i ją nazwijcie nickiem konwentowicza. To ułatwi mi potem pracę, serio!

:::warning
Każda osoba, która nagra się do HSH, może wziąć udział w losowaniu kopii anime z naszym dubbingiem. Wystarczy, że oprócz nicku poda mail, który zostanie zapisany jako nazwa ścieżki, np "Pchełka mail@nanokarrin.pl" i udostępni na fb publicznie post konkursowy Animagii. To promocja niedostępna dla członków NK, jakby co. Po konwencie odbędzie się losowanie i rozsyłanie nagród.
:::warning


##### Dziesięć piosenek
Jakie - jeszcze nie wiem. Ale muszą mieć do 2 minut, być dziecioprzyjazne i musimy mieć do nich podkład (pasujący timingowo do oryginału) i oryginał. Propozycje przyjmę.  

Dlaczego tylko 10? Bo do nich będziemy mieć przygotowane projekty w reaperze: podkłady i oryginały. Doświadczenie lat mówi nam, że pobieranie podkładów i oryginałów na bieżąco jest upierdliwe, zajmuje dużo czasu, podkłady są nieprzycięte (full ver. vs TV ver.), wygląda to bardzo nieprofesjonalnie. Jeśli ktoś będzie chciał nagrać inną piosenkę niż zaproponowane, mówimy “Przykro mi, ale nie. Przygotowaliśmy się do nagrywania tych konkretnych piosenek” i tyle. Dla upierdliwców możecie dodać “Piosenki zostały wybrane na podstawie głosowania na fb NK, zaproponuj tę piosenkę za rok :)” lub “Nie, bo lider grupy się nie zgodził, koniec kropka” - zwalcie na siłę wyższą, czyli mnie.  
Tak czy siak, jak ktoś zdecyduje się na nagranie wokalu, musicie mu wtedy powiedzieć, że jakość będzie słaba, bo jesteśmy w hali, jest głośno etc. Teksty NIE będą dostępne na kompie (bo nagrywający ma się zająć nagrywaniem, a nie scrollowaniem tekstu). Postaramy się mieć je wydrukowane, ale kartki lubią się gubić, więc w razie czego proście, by otwierali sobie teksty na smartfonach, będzie im i nam wygodniej.   
Nagrania opisujemy nickiem i po konwencie wysyłamy podkład i sam wokal. Nieczyszczony, bez achów i ochów. Niech nie piszą na fb na priva, tylko czekają, aż udostępnimy folder na fb w poście.



