---
id: prawo
title: Prawo i niesprawiedliwość
sidebar_label: Prawo i niesprawiedliwość
---

#### Umowa z Pyrkonem
Jak już kiedyś wspominałam, na stoisku nie wolno nam robić niektórych rzeczy pod karą grzywny: jeść, promować treści niezgodnych z prawem, regulaminem konwentu, obraźliwych ani rasistowskich, szkodzić wizerunkowi organizatora. Za wszelkie zniszczenia płacimy my, a na zakończenie konwentu musimy pozostawić stoisko w takim stanie, w jakim je zastaliśmy.
:::caution
Stąd też każdy dyżurujący będzie mieć minidyżur porządkowy, podczas którego **wyrzuci zalegające śmieci, wystawi nowy worek,** jeśli będzie to potrzebne, uporządkuje materiały promocyjne i kącik rysowniczy, a potem poprzyczepia co ładniejsze obrazki do galerii NK.
:::caution

#### Prawa autorskie
Gdyby ktoś pytał o prawa autorskie, to uwaga: robienie coverów w Polsce jest dozwolone i nie trzeba płacić tantiem, o ile do utworu dodaje się “własny wkład artystyczny”. W naszym przypadku główny własny wkład to język polski. My za to pieniędzy nie pobieramy (i nie będziemy), więc to powinno załatwić sprawę.  
Gdyby jednak ktoś się dopytywał, możecie zawsze odesłać go do Nano (niech wyśle mail na adres grupowy).