---
id: pk
title: Pikokoncerty
sidebar_label: Pikokoncerty
---

### 
Znów dostaliśmy pozwolenie na wykonywanie utworów na stoisku \o/. Co ok. 3 godziny możemy zaśpiewać jedną piosenkę z naszego repertuaru. Będzie to działało bardziej jako zajawka marketingowa niż pełnoprawny koncert: wokalista przychodzi, śpiewa (=przyciąga ludzi na stoisko), ukłon i wracamy do roboty. Celowałabym bardziej w atmosferę z AfterParty niż koncertową.

Godziny PK określę, kiedy ustalą się godziny warsztatów, bo te będą wpisane w program.  
Zgłoszenia wokalistów przyjmujemy przy okazji formularza zgłoszeniowego.  
Koordynatorem tego punktu programu jest Wengiel.