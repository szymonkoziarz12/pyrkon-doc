---
id: jakuczestniczyc
title: Jak mogę uczestniczyć?
sidebar_label: Jak mogę uczestniczyć?
---

Jest kilka stref, w których możesz wesprzeć Pyrkonową działalność NK.

#### Przed Pyrkonem
* Mamy już wybrane scenki, ale jedna z nich nie jest wycięta i nie ma ścieżki dźwiękowej. #NK! (niech ktoś... <3)
* Nie mamy wybranych piosenek. Będzie ich 10 i czekam na wasze propozycje. Jeśli propozycja będzie poparta podkładem i orygnałem, czas trwania max. 2 min., bezpieczna dla dzieci, to biere.
* Będziemy też robić zrzutkę $$$ na rollup oraz podstawowe dekoracje stoiska (np. materiał/papier niebieski na ścianki), żeby nie było takie gołe jak rok temu, mamy to w umowie. Info o tym pojawi się na kanale z ogłoszeniami, jak będziemy mieć oszacowane koszta.

#### Wolne Dubbingowanie
Możesz zapisać się na dyżur na stoisku, określając wcześniej, czym chcesz się zajmować bardziej:
- siedzieć w punkcie informacyjnym, udzielać informacji, zapisywać do kolejki
- być łącznikiem między punktem info a technikiem, siedzieć przy konwentowiczu, słuchać jego starań i naprowadzać go na lepszą grę aktorską (reżyserować)
- siedzieć przy komputerze i być technicznym, który pilnuje porządku w plikach, nagrywa, dba o komfort nagraniowy konwentowicza  
Każdy, kto wpisze się na dyżur, dostanie dodatkowy minidyżur porządkowy.

#### Warsztaty wokalne
Warsztaty poprowadzi Wengiel. Chodzi o podobne zabawy głosowe, jakie były na poprzednich konwentach. Potrzeba nam liderów grup wokalnych / perkusyjnej.

#### Warsztaty tekściarskie
Warsztaty poprowadzi Serek. Potrzeba ludzi z pomysłami, co wpisać w zwrotki Piosenki Pyrkonowej 2020 :D.

#### Pikokoncerty!
Dostaliśmy zgodę <3. Godziny pikokoncertów jeszcze się ustalają, bo warsztaty będą wpisane w plan na stałe i musimy się do nich dopasować, ale jeśli ktoś by chciał zaśpiewać, to zapraszam do formularza zgłoszeniowego!  
Będzie mniej slotów niż rok temu, najpewniej ok 6-7, a decyzje w sprawie pikokoncertów (kto śpiewa) podejmować będzie Wengiel.

#### Sprzęt i przydasie
Potrzebujemy ludzi, którzy dostarczą na konwent sprzęt na wolne dubbingowanie (kable, mikrofony, statywy, etc.). Dodatkowo będziemy potrzebować kredek/markerów/innych rzeczy do rysowania do Kącika Rysowniczego, nożyczek, taśmy klejącej, długopisów, kartek, worków na śmieci, foliopisów do plakatów... Jeśli możecie wziąć choć jedną z tych rzeczy - super! Wpiszcie to w formularzu!

#### Media
Fajnie byłoby mieć z Pyrkonu i filmiki, i zdjęcia, i relację na insta, i coś na fb. Jedna osoba tego nie ogarnie, a jeśli ktoś tak czy siak lubi robienie fotek, to błagam nich przemyśli robienie fotek dla NK. Nie musi to być zdjęcie stoiska jako takiego, ale też nas, ludzi, jak się dobrze bawimy, tulimy, instaramkujemy etc.

#### Po Pyrkonie
Będzie dużo pracy. Wciąż. 
* Piosenki najprawdopodobniej przygotuję ja, ale scenek ni cholery nie zrobię. Potrzebuję dzielnych śmiałków, nawet bez rangi, którzy zmontują dostarczone materiały (głosy aktorów z ambientem aka ścieżką dźwiękową). Będę o tym jeszcze pisać, ale im więcej osób się zgłosi, tym mniej scenek na osobę, wierzę w was!
* Może ktoś pokusi się o ładniejsze, "studyjne" nagranie Piosenki Pyrkonowej, wrzucimy wtedy na Spotify, jak poprzednie.


