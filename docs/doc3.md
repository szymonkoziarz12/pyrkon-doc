---
id: howtodyzur
title: Ogólne zasady dyżurów
sidebar_label: Ogólne zasady dyżurów
---

### Dbamy o bezpieczeństwo
Cały sprzęt na stoisku, tj. kable, rozgałęziacze, komputer, głośniki, mikrofon, statyw etc., należy do członków NK. Byłoby szkoda, gdyby ktoś go sobie “pożyczył”.   Nie pozwalamy obcym korzystać z laptopa, nie pozwalamy podłączać nośników, np. pendrive’ów (kiedyś w ten sposób rozprowadzano wirusa), chyba że mamy do czynienia z koordynatorem lub właścicielem sprzętu.  
Nie zostawiajcie stoiska bez opieki. Harmonogram jest jawny, wiecie, kto jest przed wami i za wami. Przed konwentem profilaktycznie weźcie do siebie numery telefonów i się kontaktujcie, gdyby zaszła taka potrzeba. Gdy wszystko zawiedzie, dzwońcie do koordynatora. Siedzicie na stoisku, dopóki ktoś was nie zmieni, więc bądźcie na czas, żeby nie zabierać go innym.  
Do krzeseł, które będą przy stoisku, pierwszeństwo mają konwentowicze, którzy się nagrywają, członek NK, który ich nagrywa oraz członkowie NK siedzący w punkcie info. Jeśli akurat nie pełnicie dyżuru, ale chcecie posiedzieć, miejcie świadomość, że może zabraknąć dla was krzesła. Nie siadajcie wtedy na stole, idźcie ukraść krzesło skądinąd ;).

### Zarządzamy przepływem ludzi
Czas, gdy zmienia się dyżur, to moment chaosu. Nie bójcie się zarządzić kilkuminutowej przerwy technicznej. Podczas przerwy nowy dyżurujący ustawia pod siebie sprzęt, a dyżurni z punktu info zajmują gości stoiska, proszą o chwilę cierpliwości i tłumaczą, co można zrobić teraz, pikokoncerty, rysunki, mapkę dubbingową... Dopiero kiedy nowi techniczni siedzą wygodnie i wiedzą, co się dzieje na kompach, prosimy konwentowiczów do nagrywania.  
Bardzo często przy napływie ludzi NK robi się zator i konwentowicze nie wiedzą, co się dzieje i czują się jak klienci w Spolemie czekający, aż eskpedient ich łaskawie zauważy i obsłuży. Takich ludzi trzeba wyłapywać.  
__Uwaga!__ Nigdy nie przerywamy nagrań z osobą, która już zaczęła, tzn. nie mówimy jej “dobra, stop, mamy przerwę, wróć za chwilę”, tylko kończymy nagrywanie na spokojnie i dopiero wtedy robimy przerwę.


### Opowiadamy o działalności NK
Mówimy pokrótce o tym, co to jest NanoKarrin, a potem od razu mówimy,  z czym przyjechaliśmy na Pyrkon i co można zrobić na stoisku. 
  Dopiero kiedy ktoś się NK zainteresuje bardziej, spyta o szczegóły, to tłumaczymy, czym się zajmujemy, gdzie można nas znaleźć, opowiadamy o możliwych do zdobycia rangach i o rekrutacji.   
Będziemy mieć także do rozdania wizytówki, ale jako że jesteśmy biedni, będzie ich mało, więc bez szaleństw.


