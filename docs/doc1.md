---
id: ogolne
title: Poznań, 8-10 maja
sidebar_label: Podstawowe informacje
---
## Gdzie?
Stoisko będzie w SFI, czyli Strefie Fantastycznych Inicjatyw, najpewniej pawilon 8. Będziemy mieć 20 metrów kwadratowych (4x5m). Między korytarzem a naszym stoiskiem będzie mała strefa buforowa, ok. 10 metrów kwadratowych, żeby nie blokować korytarza.  
Ta strefa nie należy do nas, będzie najpewniej oznaczona taśmą na podłodze. Mimo że będzie wyglądać jak nasz przedsionek, traktujcie to jako przestrzeń wspólną, nie stoisko NK.

## Godziny funkcjonowania stoiska

Piątek: 10.00-22.00  
Sobota: 10.00-22.00  
Niedziela: 10.00-18.00**  

W piątek o 10.00 stoisko powinno być w pełni sprawne i rozłożone. Koordynatorzy będą mogli wejść już w czwartek oraz w piątek ok. 8.00.
W niedzielę od 17.00 zaczynamy się zwijać.

## Koordynatorzy i numery telefonu
W tym roku mamy sześciu koordynatorów. To są ludzie, do których macie się zwracać, jeśli wystąpi jakiś problem lub jeśli będziecie mieć wątpliwość czy pytanie. Poza dyżurowaniem mają dodatkowe zadania i obowiązki.
Opiekun stoiska NK na Pyrkonie: Pchełka


| Koordynator        |      Działka      |   Numer telefonu |
| ------------- | :-----------: | -----: |
| Pchełka      | opiekun stoiska | 793 954 036 |
| Orzecho      |   dyżury, media, sprzęt    |  791 599 295 |
| Kyofu |  wystrój, branding, kącik rysowniczy    |  515 924 421 |
| Seraskus|  Piosenka Pyrkonowa, punkt informacyjny    |  694 136 892   |
| Kasar      | stanowisko do nagrywania | 667 575 511 |
| Wengiel      | pikokoncerty, warsztaty wokalne | 500 826 709 |


## Formularz i harmonogram
##### ➨➨➨ [Link do formularza zgłoszeniowego](https://forms.gle/G7KBBceqE9Gwmksj7)
Zgłoszenia do 12 kwietnia 2020 r. włącznie.

##### ➨➨➨  [Link do harmonogramu](https://docs.google.com/spreadsheets/d/1BWnk66ISz0khXQWavn3_wrRjgQb4ULi4wRaKKR9CniQ/edit?usp=sharing)
Pierwszą wersję do weryfikacji udostępnimy w połowie kwietnia. Będziemy o tym informować na dedykowanym kanale discordowym Sojuszu. 
* UWAGA! Błagam, nie przywiązujcie się do pierwszej wersji. Zwykle komuś coś nie pasuje, coś trzeba przestawić, a zmiany idą lawinowo. Czyli jeśli zgłosicie dwa okienka i wybierzemy jedno z nich, może się okazać, że przy poprawkach jednak skorzystamy z drugiego. Aż do przyklepania przez nas harmonogramu zostawcie otwarte czasowo te opcje, które nam zapisaliście w formularzu.
* Jeśli kogoś nie lubicie i nie chcecie być z nim na dyżurze, napiszcie to.
* Jeśli chcecie być z kimś na dyżurze, to dopilnujcie, byście obydwoje wpisali dane godziny i byście uzupełniali swoje role :D _Zdarzało się, że osoby z niepokrywającymi się harmonogramami chciały mieć wspólny dyżur._

## Zdjęcie grupowe
Czas do ustalenia, gdy już znane będą (stałe) godziny warsztatów. Na pewno w sobotę, raczej świeżo po południu, ale czekajcie na hasło.

## Merch NK / Koszulki
Zamówieniami merchu NK zajmę się po powrocie z urlopu, tj. ok. 13 kwietnia. Postaram się przed wyjazdem dostarczyć wam wgląd w tegoroczny wzór. Po moim powrocie terminy będą jednak krótkie, żeby się wyrobić z kurierami etc, więc nastawcie się na szybkie kwietniowe wpłaty / decyzje.  
Zamówieniem koszulek NK w tym roku zajmuje się Nashi. Informacje na ten temat będą pojawiać się na ogłoszeniach NK.